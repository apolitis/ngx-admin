/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const DepartmentsListConfiguration = {
  "title": "Admin.Groups.All",
  "model": "Departments",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ActionLinkFormatter",
      "actions": [
        {
          "title": "Admin.Users.Preview",
          "href": "#/groups/${id}",
          "role": "preview"
        },
        {
          "title": "Admin.Users.Edit",
          "href": "#/groups/${id}/edit",
          "role": "edit"
        }
      ]
    },
    {
      "name": "alternativeCode",
      "property": "alternativeCode",
      "title": "Admin.Departments.AlternativeCode"
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Departments.Name"
    },
    {
      "name": "city",
      "property": "city",
      "title": "Admin.Departments.City"
    }
  ],
  "criteria": [
    {
      "name": "alternativeCode",
      "filter": "(indexof(alternativeCode, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    }
  ],
  "paths" : [

  ]
}
