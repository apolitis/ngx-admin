/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const GroupsListConfiguration = {
  "title": "Admin.Groups.All",
  "model": "Groups",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ActionLinkFormatter",
      "actions": [
        {
          "title": "Admin.Users.Edit",
          "href": "#/groups/${id}/edit",
          "role": "edit"
        }
      ]
    },
    {
      "name": "alternateName",
      "property": "alternateName",
      "title": "Admin.Groups.AlternateName"
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Groups.Name"
    }
  ],
  "criteria": [
    {
      "name": "alternateName",
      "filter": "(indexof(alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    }
  ],
  "paths" : [

  ]
}
