/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const UserListConfiguration = {
  "title": "Admin.Users.All",
  "model": "Users",
  "searchExpression": "indexof(alternateName, '${text}') ge 0 or indexof(name, '${text}') ge 0",
  "columns": [
    {
      "name": "id",
      "property": "id",
      "formatter": "ActionLinkFormatter",
      "actions": [
        {
          "title": "Admin.Users.Edit",
          "href": "#/users/${id}/edit",
          "role": "edit"
        }
      ]
    },
    {
      "name": "alternateName",
      "property": "alternateName",
      "title": "Admin.Users.AlternateName"
    },
    {
      "name": "name",
      "property": "name",
      "title": "Admin.Users.Name"
    },
    {
      "name": "description",
      "property": "description",
      "title": "Admin.Users.Description"
    },
    {
      "name": "lastLogon",
      "property": "lastLogon",
      "title": "Admin.Users.LastLogon"
    },
    {
      "name": "enabled",
      "property": "enabled",
      "title": "Admin.Users.StatusTitle",
      "formatter": "TranslationFormatter",
      "formatString": "Admin.Users.Enabled.${value}"
    }
  ],
  "criteria": [
    {
      "name": "alternateName",
      "filter": "(indexof(alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "name",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "enabled",
      "filter": "(enabled eq '${value}')",
      "type": "text"
    }
  ],
  "paths" : [
    {
      "name": "Users.Active",
      "alternateName": "list/active",
      "show": true,
      "filter": {
        "status": "active"
      }
    },
    {
      "name": "Users.All",
      "show": true,
      "alternateName": "list/index",
      "filter": {
      }
    }
  ]
}
