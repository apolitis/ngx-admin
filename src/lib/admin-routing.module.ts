import { NgModule } from '@angular/core';
import {
  Routes,
  RouterModule
} from '@angular/router';
import {UsersComponent} from "./users/users.component";
import {UserRootComponent} from "./users/user-root/user-root.component";
import {UsersListComponent} from "./users/users-list/users-list.component";
import {UserEditComponent} from "./users/user-root/user-edit/user-edit.component";
import {UserGroupsComponent} from "./users/user-root/user-groups/user-groups.component";
import {UserDepartmentsComponent} from "./users/user-root/user-departments/user-departments.component";
import {GroupsComponent} from "./groups/groups.component";
import {GroupsListComponent} from "./groups/groups-list/groups-list.component";
import {GroupRootComponent} from "./groups/group-root/group-root.component";
import {GroupEditComponent} from "./groups/group-root/group-edit/group-edit.component";
import {GroupUsersComponent} from "./groups/group-root/group-users/group-users.component";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'users',
    pathMatch: 'full',
  },
  {
    path: 'users',
    component: UsersComponent,
    data: {
      title: 'Users'
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: UsersListComponent,
        pathMatch: 'full',
      },
      {
        path: ':id',
        component: UserRootComponent,
        data: {
          title: 'Admin.Users.User'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'edit'
          },
          {
            path: 'edit',
            component: UserEditComponent,
            data: {
              title: 'Admin.Users.Edit'
            }
          },
          {
            path: 'groups',
            component: UserGroupsComponent,
            data: {
              title: 'Admin.Users.Groups'
            }
          },
          {
            path: 'departments',
            component: UserDepartmentsComponent,
            data: {
              title: 'Admin.Users.Departments'
            }
          }
        ]
      }
    ]
  },
  {
    path: 'groups',
    component: GroupsComponent,
    data: {
      title: 'Groups'
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: GroupsListComponent,
        pathMatch: 'full',
      },
      {
        path: ':id',
        component: GroupRootComponent,
        data: {
          title: 'Admin.Groups.Group'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'edit'
          },
          {
            path: 'edit',
            component: GroupEditComponent,
            data: {
              title: 'Admin.Groups.Edit'
            }
          },
          {
            path: 'users',
            component: GroupUsersComponent,
            data: {
              title: 'Admin.Groups.Groups'
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
