export declare interface ObjectState {
  $state?: number;
}

export declare interface Thing {
     id?: number;
     additionalType?: string;
     alternateName?: string;
     description?: string;
     image?: string;
     name?: string;
     url?: string;
     dateCreated?: Date;
     dateModified?: Date;
     createdBy?: User | any;
     modifiedBy?: User | any;
}

export declare interface Account {
     accountType?: number;
}

export declare interface Group extends Account {
     members?: Array<Account|any>;
     tags?: Array<string>;
}

export declare interface Department {
     id: string;
     organization: number;
     name?: string;
     abbreviation?: string;
     city?: string;
     address?: string;
     postalCode?: string;
     country?: string;
     alternativeCode?: string;
     currentYear?: any;
     currentPeriod?: any;
     facultyName?: string;
     phone1?: string;
     phone2?: string;
     studyLevel?: any;
     contactPerson1?: string;
     contactPerson2?: string;
     email?: string;
     totalSemesters?: string;
     url?: string;
     localDepartment?: boolean;
     dateModified: Date;

}

export declare interface User extends Account {
     lockoutTime?: Date;
     logonCount?: number;
     enabled: boolean;
     lastLogon?: Date;
     groups?: Array<Group|any>;
     departments?: Array<Department | any>;
}
