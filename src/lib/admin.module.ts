import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import { AdminComponent } from './admin.component';
import { TablesModule } from '@universis/tables';
import { UsersListComponent } from './users/users-list/users-list.component';
import { AdminRoutingModule } from './admin-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { AdminService} from './admin.service';
import { environment } from './environments/environment';
import {ADMIN_LOCALES} from "./i18n";
import { UsersComponent } from './users/users.component';
import {UserRootComponent} from "./users/user-root/user-root.component";
import {CommonModule} from "@angular/common";
import { UserGroupsComponent } from './users/user-root/user-groups/user-groups.component';
import { UserDepartmentsComponent } from './users/user-root/user-departments/user-departments.component';
import { UserEditComponent } from './users/user-root/user-edit/user-edit.component';
import {UserPreviewComponent} from "./users/user-root/user-edit/user-preview/user-preview.component";
import {AdvancedFormsModule} from "@universis/forms";
import { GroupsComponent } from './groups/groups.component';
import { GroupsListComponent } from './groups/groups-list/groups-list.component';
import { GroupRootComponent } from './groups/group-root/group-root.component';
import { GroupEditComponent } from './groups/group-root/group-edit/group-edit.component';
import { GroupUsersComponent } from './groups/group-root/group-users/group-users.component';
@NgModule({
  imports: [
    TablesModule,
    AdminRoutingModule,
    TranslateModule,
    CommonModule,
    AdvancedFormsModule
  ],
  declarations: [
    AdminComponent,
    UsersListComponent,
    UserRootComponent,
    UsersComponent,
    UserGroupsComponent,
    UserDepartmentsComponent,
    UserEditComponent,
    UserPreviewComponent,
    GroupsComponent,
    GroupsListComponent,
    GroupRootComponent,
    GroupEditComponent,
    GroupUsersComponent
  ],
  exports: [AdminComponent]
})
export class AdminModule {
  constructor( @Optional() @SkipSelf() parentModule: AdminModule, private translateService: TranslateService) {
    this.ngOnInit();
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AdminModule,
      providers: [
        AdminService
      ]
    };
  }

  // tslint:disable-next-line:use-life-cycle-interface use-lifecycle-interface
  ngOnInit() {
    environment.languages.forEach( (language) => {
      if (ADMIN_LOCALES.hasOwnProperty(language)) {
        this.translateService.setTranslation(language, ADMIN_LOCALES[language], true);
      }
    });
  }

}
