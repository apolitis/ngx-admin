import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {AdvancedFormComponent} from "@universis/forms";
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {AppEventService} from "@universis/common";

@Component({
  selector: 'lib-group-edit',
  templateUrl: './group-edit.component.html'
})
export class GroupEditComponent implements OnInit {
  public group: any;
  public groupId: any;
  @Input() data: any;
  @Input() src: any;
  @ViewChild('form') form: AdvancedFormComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.groupId = this._activatedRoute.parent.snapshot.params.id
    }

    this.group = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'Group') {
        // reload request
        if (event.target.id == this.groupId) {
          this.group = event.target;

          if (this.group) {
            this.src = `Groups/edit`;
            this.data = this.group;
          }
        }
      }
    });
  }

  onCompletedSubmission($event: any) {

  }
}
