import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {AppEventService} from "@universis/common";

@Component({
  selector: 'lib-group-root',
  templateUrl: './group-root.component.html',
  styles: []
})
export class GroupRootComponent implements OnInit {
  public group: any;
  public isCreate = false;
  public config: any;
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) { }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.params.id) {
      this.group = await this._context.model('groups')
        .where('id').equal(this._activatedRoute.snapshot.params.id)
        .getItem();
      this._appEvent.change.next({
        model: 'Group',
        target: this.group
      });
    }
  }

}
