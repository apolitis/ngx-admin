import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {AdvancedTableComponent, AdvancedTableConfiguration} from "@universis/tables";
import {GroupsListConfiguration} from "../../../../assets/tables/groups/list";
import {AppEventService} from "@universis/common";

@Component({
  selector: 'lib-group-users',
  templateUrl: './group-users.component.html'
})
export class GroupUsersComponent implements OnInit {
  public group: any;
  public groupId: any;
  @ViewChild('members') members: AdvancedTableComponent;
  public filter: any = {};

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) {
  }

  async ngOnInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.groupId = this._activatedRoute.parent.snapshot.params.id
    }

    this.group = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'Group') {
        // reload request
        if (event.target.id == this.groupId) {
          this.group = event.target;

          if (this.group) {
            this.members.config = AdvancedTableConfiguration.cast(GroupsListConfiguration);
            this.members.config.model = `Groups/${this.group.id}/members`;
          }
        }
      }
    });
  }

  editCourses() {

  }
}
