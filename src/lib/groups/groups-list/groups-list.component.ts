import { Component, OnInit } from '@angular/core';
import {AdvancedTableConfiguration} from "@universis/tables";
import {GroupsListConfiguration} from "../../../assets/tables/groups/list";

@Component({
  selector: 'lib-groups-list',
  templateUrl: './groups-list.component.html'
})
export class GroupsListComponent implements OnInit {

  public readonly tableConfiguration:any = AdvancedTableConfiguration.cast(GroupsListConfiguration);
  public filter: any = {};
  constructor() { }

  ngOnInit() {
  }

}
