/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const el = {
  "Admin": {
    "Users": {
      "Title": "Χρήστης",
      "TitlePlural": "Χρήστες",
      "Enabled": {
        "true": "Ενεργός",
        "false": "Ανενεργός",
        "null": ""
      },
      "AlternateName" : "Όνομα Χρήστη",
      "Name" : "Όνομα Εισόδου",
      "Description" : "Περιγραφή",
      "LastLogon" : "Τελευταία Είσοδος",
      "StatusTitle" : "Κατάσταση",
      "Preview": "Προβολή",
      "Edit": "Επεξεργασία",
      "Id": "Κωδικός",
      "LogonCount": "Αριθμός συνδέσεων",
      "LockoutTime": "Ώρα κλειδώματος",
      "All": "Όλοι",
      "New": "Δημιουργία",
      "NewTitle": "Δημιουργία Χρήστη",
      "Export": "Εξαγωγή",
      "Type": "Τύπος",
      "View": "Προβολή",
      "Empty": "Δεν υπάρχουν εγγραφές",
      "Total": "σύνολο",
      "Info": "Στοιχεία χρήστη",
    },
    "Groups": {
      "Title" : "Ομάδα",
      "TitlePlural": "Ομάδες",
      "AlternateName" : "Εναλλακτικό Όνομα",
      "Name" : "Όνομα",
      "Edit": "Επεξεργασία",
    },
    "Departments": {
      "Title" : "Τμήματα",
      "AlternativeCode" : "Εναλλακτικός Κωδικός",
      "Name" : "Όνομα",
      "City" : "Πόλη",
    }
  }
};
