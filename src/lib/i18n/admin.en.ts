/* tslint:disable:quotemark object-literal-key-quotes object-literal-sort-keys trailing-comma */
export const en = {
  "Admin": {
    "Users": {
      "Title": "User",
      "TitlePlural": "Users",
      "Enabled": {
        "true": "Active",
        "false": "Inactive",
        "null": ""
      },
      "AlternateName" : "AlternateName",
      "Name" : "Name",
      "Description" : "Description",
      "LastLogon" : "Last Logon",
      "StatusTitle" : "Status",
      "Preview": "Preview",
      "Edit": "Edit",
      "Id": "Id",
      "LogonCount": "Logon Count",
      "LockoutTime": "Lockout Time",
      "All": "All",
      "New": "Create",
      "NewTitle": "Create User",
      "Export": "Export",
      "Type": "Type",
      "View": "View",
      "Empty": "There are no records",
      "Total": "total",
      "Info": "Info",
    },
    "Groups": {
      "Title" : "Groups",
      "TitlePlural": "Groups",
      "AlternateName" : "Alternate Name",
      "Name" : "Name",
      "Edit": "Edit",
    },
    "Departments": {
      "Title" : "Departments",
      "AlternativeCode" : "Alternative Code",
      "Name" : "Name",
      "City" : "City",
    }
  }
};
