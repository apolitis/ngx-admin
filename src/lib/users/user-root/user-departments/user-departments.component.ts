import {Component, OnInit, ViewChild} from '@angular/core';
import {AdvancedTableComponent, AdvancedTableConfiguration} from "@universis/tables";
import {ActivatedRoute} from "@angular/router";
import {AngularDataContext} from "@themost/angular";
import {DepartmentsListConfiguration} from "../../../../assets/tables/departments/list";
import {GroupsListConfiguration} from "../../../../assets/tables/groups/list";
import {AppEventService} from "@universis/common";

@Component({
  selector: 'lib-user-departments',
  templateUrl: './user-departments.component.html'
})
export class UserDepartmentsComponent implements OnInit {

  @ViewChild('departments') departments: AdvancedTableComponent;
  public filter: any = {};
  public user: any;
  public userId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _appEvent: AppEventService) { }

  async ngOnInit() {
    if (this._activatedRoute.parent.snapshot.params.id) {
      this.userId = this._activatedRoute.parent.snapshot.params.id
    }

    this.user = this._appEvent.change.subscribe(async event => {
      if (event && event.target && event.model === 'User') {
        // reload request
        if (event.target.id == this.userId) {
          this.user = event.target;

          if (this.user) {
            this.departments.config = AdvancedTableConfiguration.cast(DepartmentsListConfiguration);
            this.departments.config.model = `Users/${this.user.id}/departments`;
          }
        }
      }
    });
  }
}
